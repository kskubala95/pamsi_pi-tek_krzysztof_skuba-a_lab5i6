#include <iostream>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <cstdlib>
#include <time.h>
#include <ctime>
#include <windows.h>
#include <fstream>
// SORTOWANIE PRZEZ SCALANIE
using namespace std;

template <typename Typ>
class lexcompare 
{
	private:
		int rozmiar;
	public:	
		Typ *T,*Pom;
		int sr;
		
		
		
	Tablice(int r)
	{
	rozmiar=r;
	T=new Typ[rozmiar];
	Pom=new Typ[rozmiar];
	}

	void laczenie(int p,int sr,int k)
	{
		int i;
		int j=sr+1;
		int q=p;
		
		for (i=p; i<=k; i++)
			Pom[i]=T[i];
		i=p;
		while (i<=sr && j<=k) 
		{
			if (Pom[i]<Pom[j])
			T[q++]=Pom[i++];
			else
				T[q++]=Pom[j++];
		}
		while (i<=sr)
			T[q++]=Pom[i++];
	}
	
	void sortowanie(int p,int k)
	{
	int sr;
	if (p<k) 
	{
		sr=(p+k)/2;
		sortowanie(p, sr);
		sortowanie(sr+1, k);
		laczenie(p,sr,k);
	}	
	}
	
	void rnd()
	{
		srand((unsigned)time(NULL));
		for (long int j=0;j<rozmiar;j++)
		{
		T[j] =(rand() % 101);
		}
	}

	void wysw()
	{
		cout<<"Zawartosc tablicy"<<endl;
		for (long int j=0;j<rozmiar;j++)
		{
		cout<<T[j]<<endl;
		}
	}
	
	void odwr()
	{
		for (int j=0;j<=rozmiar;j++)
		{
			Pom[j]=T[j];
		}
		for (int k=0;k<=rozmiar;k++)
		{
		T[k]=Pom[rozmiar-k-1];
		}
	}
		
};



void menu()
{
	cout<< "1.  Wypelnij liczami losowymi ."<<endl;
	cout<< "2.  Wyswietl zawartosc."<<endl;
	cout<< "3.  Sortuj (z pomiarem czasu)."<<endl;
	cout<< "4.  Sortuj wybrany % tablicy."<<endl;
	cout<< "5.  Posortowane od tylu."<<endl;
	cout<< "6.  Sortuj ponownie losowa tablice (z pomiarem czasu)."<<endl;
	cout<< "7.  Sortuj ponownie dla posortowanego % tablicy. (z pomiarem czasu)."<<endl;
	cout<< "8.  Zapisz do pliku pomiary."<<endl;
	cout<< "9.  Czas sredni."<<endl;
	cout<< "0.  Koniec programu."<<endl;
}


main()
{
	lexcompare <int> obiekt;
	int pomiary=100;
	long double elapsedTime;
	float per;
	int rozmiar,wybor;
	float *czasy;
	czasy=new float[pomiary];
	fstream plik;

    LARGE_INTEGER t1, t2; 
	float srednia=0;
	
	menu();
	cout<< "Co chcesz zrobic ?"<<endl;
	cin>> wybor;

	while(wybor!=0)	
	{
	switch(wybor)
	{
		case 1:
		cout<< "Prosze podac ile liczb chcesz umiescic na stosie: ";
		cin>>rozmiar;
		obiekt.Tablice(rozmiar);
		obiekt.rnd();
		LARGE_INTEGER frequency;
		break;
		
		case 2:
		obiekt.wysw();
		break;
		
		case 3:
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		break;
		
		case 4:
		cout<< "Podaj ile % chcesz posortowac"<<endl;
		cin>>per;
		obiekt.sortowanie(0,(rozmiar-1)*per/100);
		break;	
		
		case 5:
		srednia=0;
		for (int it=0;it<pomiary;it++)
		{
		obiekt.rnd();	
		obiekt.odwr();
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		czasy[it]=elapsedTime;
		srednia+=czasy[it];
		}
		cout<<"Sredni czas operacji: "<< srednia/100<< " ms.\n" <<endl;
		break;
		
		case 6:
		obiekt.rnd();	
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		break;
		
		case 7:
		obiekt.rnd();	
		obiekt.sortowanie(0,(rozmiar-1)*per/100);
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		break;
		
		case 8:
		plik.open( "sprawko.txt", ios::out | ios::app);
		if( !plik.good() )
		{
    		cout << "Nie udalo sie utworzyc pliku!\n";
		}
		else
		{
    		cout << "Plik utworzony!\n";
		}
		for (int s=0;s<pomiary;s++)
		{
		obiekt.rnd();	
		obiekt.sortowanie(0,(rozmiar-1)*per/100);
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		czasy[s]=elapsedTime;
		plik<<elapsedTime<<";"<<endl;//czasy[s];
		}
		break;
		case 9:
		srednia=0;
		for (int it=0;it<pomiary;it++)
		{
		obiekt.rnd();	
		obiekt.sortowanie(0,(rozmiar-1)*per/100);
		QueryPerformanceCounter(&t1);
		obiekt.sortowanie(0,rozmiar-1);
		QueryPerformanceCounter(&t2);
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout<<"Czas operacji: "<< elapsedTime<< " ms.\n" <<endl;
		czasy[it]=elapsedTime;
		srednia+=czasy[it];
		}
		cout<<"Sredni czas operacji: "<< srednia/100<< " ms.\n" <<endl;
		break;
	
	}
	cout<< "Co chcesz zrobic ?"<<endl;
	menu();
	cin>> wybor;
	} 
	plik.close();
}
